import React from 'react';
import './App.css';
import Generate from "./components/Generate";

const App = () => {
  return (
    <div className="App">
      <Generate></Generate>
    </div>
  );
}

export default App;
