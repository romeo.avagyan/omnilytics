import React, {useState} from 'react';
import PropTypes from 'prop-types'

const Report = ({ strings, real, ints, alphanumerics }) => {
    const [showResults, setShowResults] = React.useState(false)
    const onClick = () => setShowResults(!showResults)
    return (
        <div>
            <button onClick={onClick}> Repoert </button>
            { showResults ?
                <ReportContent alphanumerics={alphanumerics} ints={ints} real={real} strings={strings}/> : null }
        </div>
    )
}

const ReportContent = ({ strings, real, ints, alphanumerics }) => {
    return (
        <div className='report'>
            <p>alphabetical strings: {strings}</p>
            <p>real numbers: {real}</p>
            <p>integers: {ints}</p>
            <p>alphanumerics: {alphanumerics}</p>
        </div>
    )
}

// Report.defaultProps = {
//     title: 'Task Tracker',
// }

// Report.propTypes = {
//     title: PropTypes.string.isRequired,
// }

export default Report