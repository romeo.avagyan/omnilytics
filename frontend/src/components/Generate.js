import React, {useState} from 'react';
import Report from "./Report";
import PropTypes from 'prop-types'

const Generate = () => {
    const [url, setUrl] = useState(null);
    const [strings, setStrings] = useState(null);
    const [real, setReal] = useState(null);
    const [ints, setInts] = useState(null);
    const [alphanumerics, setAlphanumerics] = useState(null);

    return (
        <div className='generate'>
            <button
                onClick={() => generateFile().then((res) => {
                    setUrl(res.file_url)
                    setStrings(res.random_string)
                    setAlphanumerics(res.random_alphanumerics)
                    setReal(res.random_float)
                    setInts(res.random_int)
                })}> Generate
            </button>
            <p>Link: <a href={url}>{url}</a></p>
            <Report alphanumerics={alphanumerics} ints={ints} real={real} strings={strings}/>
        </div>
    )
}

const generateFile = async () => {
    const res = await fetch('http://0.0.0.0:5000/api/files', {
        method: 'POST',
        headers: {
            'Content-type': 'application/json',
        },
        mode: 'cors'
    })
    const data = await res.json()
    return data
}


export default Generate