import datetime
import os
import random
import string
 
from flask import Flask, Response, request, send_from_directory
from flask_cors import cross_origin

app = Flask(__name__)
files_dir = 'files'

@app.route("/api")
def index():
    todos = {"title": "asd", "text": "asd", "done": False, "pub_date": datetime.datetime.now}
    return Response(todos, mimetype="application/json", status=200)

@app.route("/api/files", methods=['GET', 'POST'])
@cross_origin()
def create_file():
    file_name = f'generated-{datetime.datetime.now()}'
    file_path = os.path.join(files_dir, file_name)
    result = create_file(file_path)
    result["file_url"] = f'http://localhost:5000/api/files/{file_name}'

    # return Response({"file_url": f'/api/files/{file_name}'}, mimetype="application/json", status=200)
    return result

@app.route("/api/files/<path:filename>", methods=['GET'])
def download(filename):
    uploads = os.path.join(app.root_path, files_dir)
    return send_from_directory(directory=uploads, filename=filename)


### HELPER FUNCTIONS ###

# let's define a function for creating a string of random alphanumerical characters
def random_alphanumerics():
    length = random.randint(5, 30) # define the length of the alphanumeric string randomly between a range
    output = ''
    for i in range(length):
        output += random.choice(string.ascii_lowercase + string.digits) # create our alphanumeric string
    return output

# let's define a function for creating a string of random alphabetical characters
def random_string():
    length = random.randint(5, 30) # define the length of the alphabetical string randomly between a range
    output = ''.join(random.choice(string.ascii_lowercase) for x in range(length)) # we are using ascii lowercase so that encoding doesn't cause our file size to be unpredictable
    return output

# let's define a function for creating a random integers and converting them to a string
def random_int():
    output = random.randint(0, 10000)
    intToStr = '{}'.format(output)
    return intToStr

# let's define a function for creating random floats and converting them to a string
def random_float():
    length = random.randint(1, 10) # let's make sure the float is between a randomly chosen decimal place
    output = round(random.uniform(0.0, 10000.0), length)
    floatToStr = '{}'.format(output)
    return floatToStr

def create_file(file_name: str):
    # we shall create and open our file
    open(file_name, 'w')

    # let's check the initial size of the file which should be 0
    fileSize = os.stat(file_name).st_size

    stats = {
        "random_alphanumerics": 0,
        "random_string": 0,
        "random_int": 0,
        "random_float": 0
    }

    # alright let's open the file and append data to it
    with open(file_name, 'a') as myFile:
        while fileSize < 2097152: # run the loop until fileSize is 2097152 bytes which should be shown as 2MB in any OS
            function_list = [random_alphanumerics, random_string, random_int, random_float] # put our functions into a list
            dataType = random.choice(function_list) # randomly choose a function to run
            stats[dataType.__name__] += 1
            if dataType == random_alphanumerics: # our alphanumeric string needs to have whitespaces before and after it so let's use an if statement for that
                output = random_alphanumerics()
                output = output + ' '
            else:
                output = dataType()
            myFile.write(output + ', ')
            fileSize = os.stat(file_name).st_size
        # once loop is done, print final file size and close file
        myFile.close()
    return stats

if __name__ == "__main__":
    app.run(debug=True, port=5000)